//\kyc контракт верси 02
// задача данного контракта - сохранять присланные ему значения (адреса будущих инвесторов)
// входящий параметр "адрес"
// ответ от контракта false или true 

pragma solidity ^0.4.18;

contract KYC{
    address creator;                     //храним адреса создателя контракта
    mapping(address => bool) approved;   //словарь со списком белх адресов
    mapping(address => bool) operators;

    function KYC(){
        creator = msg.sender;
    }
    function setOperator(address newOperator) {
        require(msg.sender==creator);
        operators[newOperator] = true;
    }

    function denyOperator(address operator) {
        require(msg.sender==creator);
        operators[operator] = false;
    }
    function getOperator(address isOperator) constant returns (bool){
        return operators[isOperator];
    }
    function setApprove(address approvedAddress) {
        require(getOperator(msg.sender));
        approved[approvedAddress] = true;
    }
    function denyApprove(address approvedAddress) {
        require(getOperator(msg.sender));
        approved[approvedAddress] = false;
    }
    function getApprove(address approveRequest) constant returns (bool){
        return approved[approveRequest];
    }
    function creatorChange(address newCreator){
        require(msg.sender == creator);
        require(creator != address(0));
        creator = newCreator;
    }
}
